<?php

namespace App\Http\Controllers;
use DB;
use App\Menu;
use App\Restaurant;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

class ReservationController extends Controller
{
    //
    public function index()
    {
        return view('reservation.reservation');
    }

    public function reservationForm()
    {
        return view('reservation.reservation-form');
    }

    public function viewReservation(){
        $reservation = Reservation::all();
        $message = 'success';

        $data = array();
        //dd($program);
        if(isset($reservation))
        {
            foreach($reservation as $a)
            {
                $array = array();
               /* $array['edit'] = '<span class="server-maintenance-button" data-toggle="modal" data-target="#add-modal"><button class="btn btn-xs btn-primary" id="editUser" name="editUser" value='.$a->id.'>
                <i class="fa fa-pencil"></i> EDIT</button></span>';*/
                $id = $a->id;
                $array['name'] = $a->customerName;
                $array['contact'] = $a->contact;
                $array['orders'] = $a->orders;
                $array['numSeats'] = $a->numSeats;
                $array['desiredTime'] = $a->desiredTime;
                $array['desiredDate'] = $a->desiredDate;
                $array['specialRequest'] = $a->specialRequest;
                $array['status'] = $a->status;
                $array['amount'] = $a->totalAmount;
                $array['button'] = "";
                //<button class='AddToCart btn btn-primary add_btn' value = '$id' onclick='myFunction(this, $id, $idresto)'>Add To Cart</button>

                if($a->status == "Pending"){
                    $array['button'] = "<button id = 'Approve' value = '$id' onclick='approveFunction($id)' type='button' class='btn btn-primary'                       data-target='.bd-example-modal-lg'>APPROVE RESERVATION</button><br></br>
                                    <button id = 'Decline' value = '$id' onclick='declineFunction($id)' type='button' class='btn btn-primary' data-target='.bd-example-modal-lg'>DECLINE RESERVATION</button>";
                }

                if($a->status == "Approved"){
                    $array['button'] = "<button id = 'Decline' value = '$id' onclick='declineFunction($id)' type='button' class='btn btn-primary' data-target='.bd-example-modal-lg'>DECLINE RESERVATION</button>";
                }

                if($a->status == "Declined"){
                    $array['button'] = "<button id = 'Approve' value = '$id' onclick='approveFunction($id)' type='button' class='btn btn-primary'                       data-target='.bd-example-modal-lg'>APPROVE RESERVATION</button>";
                }
                
                $data[] = $array;
            }
        }

        $data = array('data' => $data, 'message' =>$message);

        return json_encode($data);
    }

    public function approve(Request $request){
        
        $id = $request->query('id');
        $reservation = Reservation::find($id);
        $reservation->status = "Approved";
        $reservation->save();

    }

    public function decline(Request $request){
        
        $id = $request->query('id');
        $reservation = Reservation::find($id);
        $reservation->status = "Declined";
        $reservation->save();

    }

    public function customer_reservation()
    {
        session_start();
        if(empty($_SESSION['food']) || empty($_SESSION['quantity']))
        {
            $_SESSION['food'] = array();
            $_SESSION['quantity'] = array();
            echo "empty session <br>";
        }
        $food = $_SESSION['food'];
        $size = $_SESSION['quantity'];

        $str = implode(" or menu.id = ", $food);
        $str2 = implode(",", $food);
        
        $menu = DB::select(DB::raw("select menu.id as menuid, restaurants.id as restoid, restaurants.name as restoname, menu.name as menuname, menu.price as menuprice, menu.isAvailable as availability from menu inner join restaurants on restaurants.id = menu.restaurant where menu.id = $str order by field(menu.id, $str2)"));
       // dd($menu);
        $data = array();
        $message = 'success';
        $i = 0;
        $order = array();
        $totalAmount = 0;
        foreach($menu as $m){
            $array = array();
            $menuid = $m->menuid;
            $array['food'] = "";
            $sort = sort($food);
            //echo $menuid;
            $array['food'] = $m->menuname;
            $array['quantity'] = $size[$i];
            //print_r($size[$i]);
            $array['price'] = $size[$i] * $m->menuprice;
            $totalAmount += $array['price'];
            array_push($order, $m->menuname." - ".$size[$i]." - ".($size[$i] * $m->menuprice));
            $i++;
            
            $data[] = $array;
         //   }
            
        }

        $order_add = implode(" : ", $order);
        $_SESSION['order'] = $order_add;
        $_SESSION['orderAmount'] = $totalAmount;
        //print_r($order_add);
        
        
        $data = array('data' => $data, 'message' =>$message);
        return json_encode($data);
    }

    public function create(Request $request){
        
        session_start();
        $order = $_SESSION['order'];
        $reservation = new Reservation;
        $name = \Auth::user()->name;
        $contact = \Auth::user()->contact;
        $reservation->customerName = $name;
        $reservation->contact = $contact;
        $reservation->orders = $order;
        $reservation->numSeats = $request->numSeats;
        $reservation->desiredTime = $request->desiredTime;
        $reservation->desiredDate = $request->desiredDate;
        $reservation->totalAmount = $_SESSION['orderAmount'];
        $reservation->specialRequest = $request->specialRequest;
        $reservation->status = "Pending";
        session_destroy();
        $reservation->save();
        return Redirect::back()->with(['success', 'Reservation Created Successfully']);
    }

    public function all()
    {
        $restaurant = Restaurant::all();
        $message = 'success';

        $data = array();
        //dd($program);
        if(isset($restaurant))
        {
            foreach($restaurant as $a)
            {
                $array = array();
               /* $array['edit'] = '<span class="server-maintenance-button" data-toggle="modal" data-target="#add-modal"><button class="btn btn-xs btn-primary" id="editUser" name="editUser" value='.$a->id.'>
                <i class="fa fa-pencil"></i> EDIT</button></span>';*/
                $array['food'] = $a->name;
                $array['quantity'] = $a->description;
                $array['amount'] = $a->address;
                $data[] = $array;
            }
        }

        $data = array('data' => $data, 'message' =>$message);

        return json_encode($data);
    }
}
